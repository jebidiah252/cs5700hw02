﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace CS5700HW02
{
    public class StockPortfolio : Dictionary<string, Stock>
    {
        public void Update(TickerMessage message)
        {
            if (message == null) return;

            if (ContainsKey(message.Symbol))
                this[message.Symbol].Update(message);
        }

        public Stock FindValue(string key)
        {
            return this[key];
        }

        public List<string> GetKeys()
        {
            List<string> keys = new List<string>();

            foreach(KeyValuePair<string, Stock> entry in this)
            {
                keys.Add(entry.Key);
            }
            return keys;
        }
    }
}
