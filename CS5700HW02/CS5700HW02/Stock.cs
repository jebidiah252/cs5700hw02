﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS5700HW02
{
    public class Stock
    {
        // TODO: Design and implement the properties and behaviors of this class.
        public string Symbol { get; set; }
        public int OpeningPrice { get; set; }
        public int PreviousClosingPrice { get; set; }
        public int CurrentPrice { get; set; }
        public int BidPrice { get; set; }
        public int AskPrice { get; set; }
        public int CurrentVolume { get; set; }
        public int AverageVolume { get; set; }

        public void Update(TickerMessage message)
        {
            // TODO: Update the state of the stock object
            Symbol = message.Symbol;
            OpeningPrice = message.OpeningPrice;
            PreviousClosingPrice = message.PreviousClosingPrice;
            CurrentPrice = message.CurrentPrice;
            BidPrice = message.BidPrice;
            AskPrice = message.AskPrice;
            CurrentVolume = message.CurrentVolume;
            AverageVolume = message.AverageVolume;
        }
    }
}
