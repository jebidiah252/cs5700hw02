﻿namespace CS5700HW02
{
    partial class ControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.observersLabel = new System.Windows.Forms.Label();
            this.observersListView = new System.Windows.Forms.ListView();
            this.createObserverButton = new System.Windows.Forms.Button();
            this.observerDeleteButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.stockVolumeGraph = new System.Windows.Forms.CheckBox();
            this.stockPricePanel = new System.Windows.Forms.CheckBox();
            this.stockPriceGraph = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.loadCSVFileButton = new System.Windows.Forms.Button();
            this.saveCSVFileButton = new System.Windows.Forms.Button();
            this.CSVFileNameLabel = new System.Windows.Forms.Label();
            this.IPSelection = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // observersLabel
            // 
            this.observersLabel.AutoSize = true;
            this.observersLabel.Location = new System.Drawing.Point(13, 13);
            this.observersLabel.Name = "observersLabel";
            this.observersLabel.Size = new System.Drawing.Size(58, 13);
            this.observersLabel.TabIndex = 0;
            this.observersLabel.Text = "Observers:";
            // 
            // observersListView
            // 
            this.observersListView.Location = new System.Drawing.Point(13, 30);
            this.observersListView.Name = "observersListView";
            this.observersListView.Size = new System.Drawing.Size(251, 125);
            this.observersListView.TabIndex = 1;
            this.observersListView.UseCompatibleStateImageBehavior = false;
            // 
            // createObserverButton
            // 
            this.createObserverButton.Location = new System.Drawing.Point(13, 161);
            this.createObserverButton.Name = "createObserverButton";
            this.createObserverButton.Size = new System.Drawing.Size(75, 23);
            this.createObserverButton.TabIndex = 2;
            this.createObserverButton.Text = "Create";
            this.createObserverButton.UseVisualStyleBackColor = true;
            this.createObserverButton.Click += new System.EventHandler(this.createObserverButton_Click);
            // 
            // observerDeleteButton
            // 
            this.observerDeleteButton.Location = new System.Drawing.Point(189, 161);
            this.observerDeleteButton.Name = "observerDeleteButton";
            this.observerDeleteButton.Size = new System.Drawing.Size(75, 23);
            this.observerDeleteButton.TabIndex = 3;
            this.observerDeleteButton.Text = "Delete";
            this.observerDeleteButton.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.stockVolumeGraph);
            this.groupBox1.Controls.Add(this.stockPricePanel);
            this.groupBox1.Controls.Add(this.stockPriceGraph);
            this.groupBox1.Location = new System.Drawing.Point(332, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 89);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "View Options";
            // 
            // stockVolumeGraph
            // 
            this.stockVolumeGraph.AutoSize = true;
            this.stockVolumeGraph.Location = new System.Drawing.Point(7, 66);
            this.stockVolumeGraph.Name = "stockVolumeGraph";
            this.stockVolumeGraph.Size = new System.Drawing.Size(124, 17);
            this.stockVolumeGraph.TabIndex = 2;
            this.stockVolumeGraph.Text = "Stock Volume Graph";
            this.stockVolumeGraph.UseVisualStyleBackColor = true;
            // 
            // stockPricePanel
            // 
            this.stockPricePanel.AutoSize = true;
            this.stockPricePanel.Location = new System.Drawing.Point(7, 43);
            this.stockPricePanel.Name = "stockPricePanel";
            this.stockPricePanel.Size = new System.Drawing.Size(111, 17);
            this.stockPricePanel.TabIndex = 1;
            this.stockPricePanel.Text = "Stock Price Panel";
            this.stockPricePanel.UseVisualStyleBackColor = true;
            // 
            // stockPriceGraph
            // 
            this.stockPriceGraph.AutoSize = true;
            this.stockPriceGraph.Location = new System.Drawing.Point(7, 20);
            this.stockPriceGraph.Name = "stockPriceGraph";
            this.stockPriceGraph.Size = new System.Drawing.Size(113, 17);
            this.stockPriceGraph.TabIndex = 0;
            this.stockPriceGraph.Text = "Stock Price Graph";
            this.stockPriceGraph.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Location = new System.Drawing.Point(13, 246);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(251, 266);
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Stock ID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Symbol";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.listView2.Location = new System.Drawing.Point(332, 246);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(271, 266);
            this.listView2.TabIndex = 6;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Stock ID";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Symbol";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Create Panel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // loadCSVFileButton
            // 
            this.loadCSVFileButton.Location = new System.Drawing.Point(339, 156);
            this.loadCSVFileButton.Name = "loadCSVFileButton";
            this.loadCSVFileButton.Size = new System.Drawing.Size(85, 23);
            this.loadCSVFileButton.TabIndex = 8;
            this.loadCSVFileButton.Text = "Load CSV File";
            this.loadCSVFileButton.UseVisualStyleBackColor = true;
            // 
            // saveCSVFileButton
            // 
            this.saveCSVFileButton.Location = new System.Drawing.Point(339, 186);
            this.saveCSVFileButton.Name = "saveCSVFileButton";
            this.saveCSVFileButton.Size = new System.Drawing.Size(85, 23);
            this.saveCSVFileButton.TabIndex = 9;
            this.saveCSVFileButton.Text = "Save CSV File";
            this.saveCSVFileButton.UseVisualStyleBackColor = true;
            // 
            // CSVFileNameLabel
            // 
            this.CSVFileNameLabel.AutoSize = true;
            this.CSVFileNameLabel.Location = new System.Drawing.Point(339, 216);
            this.CSVFileNameLabel.Name = "CSVFileNameLabel";
            this.CSVFileNameLabel.Size = new System.Drawing.Size(79, 13);
            this.CSVFileNameLabel.TabIndex = 10;
            this.CSVFileNameLabel.Text = "CSV Filename: ";
            // 
            // IPSelection
            // 
            this.IPSelection.Location = new System.Drawing.Point(527, 126);
            this.IPSelection.Name = "IPSelection";
            this.IPSelection.Size = new System.Drawing.Size(75, 23);
            this.IPSelection.TabIndex = 11;
            this.IPSelection.Text = "Change IP";
            this.IPSelection.UseVisualStyleBackColor = true;
            this.IPSelection.Click += new System.EventHandler(this.IPSelection_Click);
            // 
            // ControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 524);
            this.Controls.Add(this.IPSelection);
            this.Controls.Add(this.CSVFileNameLabel);
            this.Controls.Add(this.saveCSVFileButton);
            this.Controls.Add(this.loadCSVFileButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.observerDeleteButton);
            this.Controls.Add(this.createObserverButton);
            this.Controls.Add(this.observersListView);
            this.Controls.Add(this.observersLabel);
            this.Name = "ControlForm";
            this.Text = "Control Form";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label observersLabel;
        private System.Windows.Forms.ListView observersListView;
        private System.Windows.Forms.Button createObserverButton;
        private System.Windows.Forms.Button observerDeleteButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox stockVolumeGraph;
        private System.Windows.Forms.CheckBox stockPricePanel;
        private System.Windows.Forms.CheckBox stockPriceGraph;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button loadCSVFileButton;
        private System.Windows.Forms.Button saveCSVFileButton;
        private System.Windows.Forms.Label CSVFileNameLabel;
        private System.Windows.Forms.Button IPSelection;
    }
}

