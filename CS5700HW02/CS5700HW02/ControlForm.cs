﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CS5700HW02
{
    public partial class ControlForm : Form
    {
        public ObserverForm obForm { get; set; }
        public ControlForm()
        {
            StartPosition = FormStartPosition.Manual;
            Location = new Point(850, 400);
            InitializeComponent();
            obForm = new ObserverForm();
            obForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void IPSelection_Click(object sender, EventArgs e)
        {
            IPAddressForm theForm = new IPAddressForm();
            if(theForm.ShowDialog() == DialogResult.OK)
            {
                obForm.IPAddress = theForm.IPAddress;
            }
            obForm.StartReading();
        }

        private void createObserverButton_Click(object sender, EventArgs e)
        {

        }
    }
}
