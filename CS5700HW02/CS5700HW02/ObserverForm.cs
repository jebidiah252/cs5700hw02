﻿using System;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CS5700HW02
{
    public partial class ObserverForm : Form
    {
        public PriceGraph priceGraph;
        public VolumeGraph volumeGraph;
        public PricePanel pricePanel;

        // create a list of known stocks
        public StockPortfolio portfolio = new StockPortfolio();

        private Communicator communicator { get; set; }
        public bool isRunning = false;
        private int secondsRunning = 0;

        public string IPAddress { get; set; }
        public ObserverForm()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            IsMdiContainer = true;

            portfolio.Add("AAPL", new Stock() { });
            portfolio.Add("AMZN", new Stock() { });
            portfolio.Add("GOOGL", new Stock() { });
            portfolio.Add("MSFT", new Stock() { });

            priceGraph = new PriceGraph();
            volumeGraph = new VolumeGraph();
            pricePanel = new PricePanel();

            priceGraph.StartPosition = FormStartPosition.Manual;
            priceGraph.Location = new Point(5, 5);

            volumeGraph.StartPosition = FormStartPosition.Manual;
            volumeGraph.Location = new Point(950, 5);

            pricePanel.StartPosition = FormStartPosition.Manual;
            pricePanel.Location = new Point(595, 5);

            priceGraph.MdiParent = this;
            volumeGraph.MdiParent = this;
            pricePanel.MdiParent = this;

            priceGraph.Show();
            volumeGraph.Show();
            pricePanel.Show();


        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            if(IPAddress != null && isRunning)
            {
                pricePanel.ClearForm();
                List<string> keys = portfolio.GetKeys();
                secondsRunning++;
                foreach (string key in keys)
                {
                    pricePanel.UpdateForm(portfolio, key);
                    if (key == priceGraph.Key)
                    {
                        priceGraph.UpdateGraph(portfolio);
                    }
                }
            }
        }

        public void StartReading()
        {
            if (IPAddress != null && !isRunning)
            {
                IPEndPoint simulatorEndPoint = EndPointParser.Parse(IPAddress);

                communicator = new Communicator() { Portfolio = portfolio, RemoteEndPoint = simulatorEndPoint };
                MessageBox.Show("Starting to read values");
                isRunning = true;
                communicator.Start();
            }
            else
            {
                MessageBox.Show("There was an error reading IP Address. Try typing in a new one");
            }
        }
    }
}
