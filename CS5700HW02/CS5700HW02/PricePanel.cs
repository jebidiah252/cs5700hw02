﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CS5700HW02
{
    public partial class PricePanel : Form
    {
        public PricePanel()
        {
            InitializeComponent();
            portfolioListView.View = View.Details;
            portfolioListView.FullRowSelect = true;
        }

        public void UpdateForm(StockPortfolio portfolio, string key)
        {
            if(portfolio.ContainsKey(key))
            {
                Stock myStock = portfolio.FindValue(key);
                string[] row = { myStock.CurrentPrice.ToString(), myStock.BidPrice.ToString(), myStock.AskPrice.ToString() };
                portfolioListView.Items.Add(key).SubItems.AddRange(row);
            }
        }

        public void ClearForm()
        {
            portfolioListView.Items.Clear();
        }
    }
}
