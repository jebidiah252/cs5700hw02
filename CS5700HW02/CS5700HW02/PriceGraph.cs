﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CS5700HW02
{
    public partial class PriceGraph : Form
    {
        public PriceGraph()
        {
            InitializeComponent();
        }
        public string Key { get; set; }
        public Series series1;
        private bool changeYScale = false;
        private void PriceGraph_Load(object sender, EventArgs e)
        {
            CreateChart("AAPL");
        }

        private int increment = 0;
        public void UpdateGraph(StockPortfolio portfolio)
        {
            if(portfolio.ContainsKey(Key))
            {
                if (increment > 200)
                {
                    series1.Points.Clear();
                    increment = 0;
                    changeYScale = true;
                }
                Stock myStock = portfolio.FindValue(Key);
                if(changeYScale)
                {
                    chart1.ChartAreas[0].AxisY.Maximum = myStock.AskPrice + 500;
                    if (myStock.AskPrice >= 500)
                        chart1.ChartAreas[0].AxisY.Minimum = myStock.AskPrice - 500;
                    else
                        chart1.ChartAreas[0].AxisY.Minimum = 0;
                    changeYScale = false;
                }
                series1.Points.AddXY(increment++, myStock.AskPrice);
            }
            chart1.Refresh();
            chart1.Invalidate();
        }

        public void CreateChart(string key)
        {
            chart1.Series.Clear();
            Key = key;
            series1 = new Series
            {
                Name = key,
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
            };

            this.chart1.Series.Add(series1);
        }
    }
}
