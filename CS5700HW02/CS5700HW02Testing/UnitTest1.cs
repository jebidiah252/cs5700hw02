﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CS5700HW02;

namespace CS5700HW02Testing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Communicator comm = new Communicator();
            comm.Start();
            Assert.IsNotNull(comm);
        }

        [TestMethod]
        public void TestMethod2()
        {
            PriceGraph graph = new PriceGraph();
            graph.CreateChart("AAPL");
            Assert.IsNotNull(graph.series1);
        }

        [TestMethod]
        public void TestMethod3()
        {
            PricePanel panel = new PricePanel();
            panel.ClearForm();
            Assert.IsNotNull(panel);
        }
    }
}
